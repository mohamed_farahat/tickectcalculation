package com.example.farahat.ticketcalculator;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import org.angmarch.views.NiceSpinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    NiceSpinner startSpinner, endSpinner;
    String start;
    String end;
    TextView stationNumber, ticketprice;

    List<String> stationsAlph = new LinkedList<>(Arrays.asList("ارض المعارض", "الأهرام",
            "الأوبرا", "البحوث", "الجيزة", "الجيش",
            "الخلفاوى", "الدقى", "الدمرداش", "الزهراء"
            , "السادات", "السيدة زينب", "الشهداء",
            "العباسية", "العتبة", "المرج",
            "المرج الجديدة", "المطرية", "المظلات",
            "المعادى", "المعصرة",
            "الملك الصالح", "المنيب", "أم المصريين",
            "باب الشعرية", "ثكنات المعادى", "جامعة القاهرة", "جامعة حلوان",
            "حدائق الزيتون", "حدائق المعادى", "حدائق حلوان", "حلمية الزيتون", "حلوان",
            "حمامات القبة", "دار السلام", "روض الفرج",
            "ساقية مكى", "سانتا تريزا", "ستاد القاهرة", "سراى القبة",
            "سعد زعلول", "شبرا الخيمة", "طرة الاسمنت", "طرة البلد", "عبده باشا", "عرابى",
            "عزبه النخل", "عين حلوان", "عين شمس", "غمرة", "فيصل", "كلية البنات",
            "كلية الزراعة", "كوبرى القبة", "كوتسيكا", "مارجرجس", "محمد نجيب", "مسرة", "منشية الصدر", "ناصر", "وادى حوف"));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startSpinner = (NiceSpinner) findViewById(R.id.start_spinner);
        endSpinner = (NiceSpinner) findViewById(R.id.end_spinner);
        stationNumber = (TextView) findViewById(R.id.count);
        ticketprice = (TextView) findViewById(R.id.price);

        startSpinner.attachDataSource(stationsAlph);
        endSpinner.attachDataSource(stationsAlph);
        startSpinner.addOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                for (int x = 0; x <= stationsAlph.size(); x++) {
                    if (x == position) {
                        Toast.makeText(MainActivity.this, stationsAlph.get(position), Toast.LENGTH_SHORT).show();
                        start = stationsAlph.get(position);
                        break;
                    }
                }
            }
        });
        endSpinner.addOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int x = 0; x <= stationsAlph.size(); x++) {
                    if (x == position) {
                        Toast.makeText(MainActivity.this, stationsAlph.get(position), Toast.LENGTH_SHORT).show();
                        end = stationsAlph.get(position);
                        break;
                    }
                }
            }
        });
    }


    public void caluclateTickect(View view) {

        new calulateStation().execute(start, end);
    }
    //Line 1 {0:34}
    //Line 2 {35:52}
    //Line 3 {53:60}


    private class calulateStation extends AsyncTask<String, Void, String> {
        List<String> firstLineStations = new LinkedList<>(Arrays.asList(
                "حلوان", "عين حلوان", "جامعة حلوان", "وادى حوف", "حدائق حلوان", "المعصرة", "طرة الاسمنت", "كوتسيكا", "طرة البلد", "ثكنات المعادى", "المعادى", "حدائق المعادى", "دار السلام", "الزهراء", "مارجرجس", "الملك الصالح", "السيدة زينب", "سعد زعلول", "السادات", "ناصر", "عرابى", "الشهداء", "غمرة", "الدمرداش", "منشية الصدر", "كوبرى القبة", "حمامات القبة", "سراى القبة", "حدائق الزيتون", "حلمية الزيتون", "المطرية", "عين شمس", "عزبة النخل", "المرج", "المرج الجديدة",
                "المنيب", "ساقية مكى", "أم المصريين", "الجيزة", "فيصل", "جامعة القاهرة", "البحوث", "الدقى", "الأوبرا", "محمد نجيب", "العتبة", "مسرة", "روض الفرج", "سانتا تريزا", "الخلفاوى", "المظلات", "كلية الزراعة", "شبرا الخيمة",
                "باب الشعرية", "الجيش", "عبده باشا", "العباسية", "ارض المعارض", "ستاد القاهرة", "كلية البنات", "الأهرام"));


        @Override
        protected String doInBackground(String... params) {
            String startBack = params[0];
            String endBack = params[1];
            int stationsNum;
            int s = 0, e = 0;
            try {
                for (int i = 0; i <= firstLineStations.size(); i++) {
                    if (firstLineStations.get(i) == startBack) {
                        s = i;
                        break;
                    }
                }
                for (int i = 0; i <= firstLineStations.size(); i++) {
                    if (firstLineStations.get(i) == endBack) {
                        e = i;
                        break;
                    }
                }
            } catch (Exception ex) {
//                Log.e("BackExcption", ex.getMessage());
            }


            return String.valueOf(findNumberOfStations(s, e));
        }

        @Override
        protected void onPostExecute(String result) {
            stationNumber.setText(result);

        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public int findNumberOfStations(int start, int end) {
        int result = 0;
        Log.e("sss", String.valueOf(start));
        Log.e("eee", String.valueOf(end));
        if (start >= 0 && start <= 34) {
            result = handleLine1(start, end);
        }
        if (start >= 35 && start <= 52) {
            result = handleLine2(start, end);
        }
        if (start >= 53 && start <= 60) {
            result = handleLine3(start, end);
        }
        return result;


    }

    private int handleLine1(int start, int end) {
        int FromstartToSadat;
        int FromSatadToEnd;
        int line1Result = 0;
        if (end >= 0 && end <= 34) {
            line1Result = end - start;
        }
        if (end >= 35 && end <= 43) {
            FromSatadToEnd = 9 - (end - 35);
            FromstartToSadat = handleMinus(18 - start);
            line1Result = FromstartToSadat + FromSatadToEnd;
        }
        if (end >= 44 && end <= 53) {
            if (end >= 44 && end <= 45) {
                if (start <= 19) {
                    FromstartToSadat = handleMinus(18 - start);
                    FromSatadToEnd = end - 43;
                    line1Result = FromstartToSadat + FromSatadToEnd;
                }
                if (start > 19 && start <= 34) {
                    if (start == 20) {
                        Log.e("ORABI", "Yesin");
                        if (end == 44) {
                            FromstartToSadat = handleMinus(21 - start);
                            line1Result = FromstartToSadat + 2;
                        } else {
                            FromstartToSadat = handleMinus(21 - start);
                            FromSatadToEnd = end - 44;
                            line1Result = FromstartToSadat + FromSatadToEnd;
                        }

                    }
                    if (start > 20) {
                        if (end == 44) {
                            FromstartToSadat = handleMinus(21 - start);
                            line1Result = FromstartToSadat + 2;
                        }
                        if (end == 45) {
                            FromstartToSadat = handleMinus(21 - start);
                            line1Result = FromstartToSadat + 1;

                        }
                    }
                }
            }
            if (end >= 46 && end <= 53) {
                if (start >= 18) {
                    FromstartToSadat = handleMinus(21 - start);
                    FromSatadToEnd = end - 45;
                    line1Result = FromstartToSadat + FromSatadToEnd;
                }
                if (start < 18) {
                    FromstartToSadat = handleMinus(18 - start);
                    FromSatadToEnd = (end - 45) + 3;
                    line1Result = FromstartToSadat + FromSatadToEnd;
                }
            }
        }
        if (end >= 53 && end <= 60) {
            line1Result = handleMinus(18 - start) + 2 + (8 - handleMinus(60 - end));
        }
        return handleMinus(line1Result);
    }

    private int handleLine2(int start, int end) {
        int line2Result = 0;
        //same line
        int oberaStationIndex = 43;
        if (start >= 35 && start <= 43) {
            if (end >= 0 && end <= 18) {
                line2Result = (9 - (handleMinus(start - 35))) + handleMinus(18 - end);
            }
            if (end >= 22 && end <= 34) {
                line2Result = (9 - (handleMinus(start - 35))) + 3 + (13 - (34 - end));
            }
            if (end >= 53 && end <= 60) {
                line2Result = (9 - (handleMinus(start - 35))) + 2 + (8 - handleMinus(60 - end));
                Log.e("SSSSSSS", String.valueOf(end));
            }
            if (end >= 35 && end <= 43) {
                line2Result = handleMinus(end - start);
            }
            if (end == 18) {
                line2Result = handleMinus(oberaStationIndex - (start)) + 1;
            }
            if (end == 44 || end == 19) {
                line2Result = handleMinus(oberaStationIndex - (start)) + 2;
            }
            if (end == 45 || end == 20) {
                line2Result = handleMinus(oberaStationIndex - (start)) + 3;
            }
            if (end == 21) {
                line2Result = handleMinus(oberaStationIndex - (start)) + 4;
            }
            if (end >= 46 && end <= 52) {
                line2Result = handleMinus(end - start) + 2;
            }
        }
        if (start == 44) {
            if (end >= 35 && end <= 43) {
                line2Result = 9 - handleMinus(end - 35) + 1;
            }
            if (end == 18 || end == 45) {
                line2Result = 1;
            }
            if (end == 20) {
                line2Result = 3;
            }
            if (end == 19 || end == 21) {
                line2Result = 2;
            }
            if (end >= 46 && end <= 52) {
                line2Result = 7 - (52 - end) + 2;
            }
        }
        if (start == 45) {
            if (end >= 35 && end <= 43) {
                line2Result = 9 - handleMinus(end - 35) + 2;
            }
            if (end == 44 || end == 21) {
                line2Result = 1;
            }
            if (end == 18 || end == 20) {
                line2Result = 2;
            }
            if (end == 19) {
                line2Result = 3;
            }
            if (end >= 46 && end <= 52) {
                line2Result = handleMinus(end - start) + 1;
            }
        }
        if (start >= 46 && start <= 52) {
            if (end >= 46 && start <= 52) {
                line2Result = handleMinus(end - start);
            }
            if (end == 44 || end == 45) {
                line2Result = handleMinus(end - start) + 1;
            }
            if (end >= 35 && end <= 43) {
                line2Result = handleMinus(end - start) + 2;
            }
        }

        return line2Result;
    }

    private int handleLine3(int start, int end) {
        int line3Result = 0;
        if (end >= 53 && end <= 60) {
            line3Result = handleMinus(end - start);
        }
        if (end == 45) {
            line3Result = handleMinus((8 - (60 - start)));
        }
        if (end == 44 || end == 21) {
            line3Result = handleMinus((8 - (60 - start))) + 1;
        }
        if (end == 18 || end == 20) {
            line3Result = handleMinus((8 - (60 - start))) + 2;
        }
        if (end == 19) {
            line3Result = handleMinus((8 - (60 - start))) + 3;
        }
        if (end >= 0 && end <= 17) {
            line3Result = ((8 - (60 - start))) + (17 - end) + 3;
        }
        if (end >= 35 && end <= 43) {
            line3Result = ((8 - (60 - start))) + (9 - (handleMinus(end - 35))) + 2;
        }
        if (end >= 0 && end <= 17) {
            line3Result = ((8 - (60 - start))) + (18 - end) + 2;
        }
        if (end >= 20 && end <= 34) {
            line3Result = ((8 - (60 - start))) + (13 - (handleMinus(34 - end))) + 1;
        }
        return line3Result;
    }

    private int handleMinus(int i) {
        if (i < 0)
            return i * (-1);
        else
            return i;
    }
}